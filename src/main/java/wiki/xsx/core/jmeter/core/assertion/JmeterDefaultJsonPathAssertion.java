package wiki.xsx.core.jmeter.core.assertion;

import wiki.xsx.core.jmeter.core.util.JmeterOptional;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.jmeter.assertions.Assertion;
import org.apache.jmeter.assertions.JSONPathAssertion;
import org.apache.jmeter.assertions.gui.JSONPathAssertionGui;
import org.apache.jmeter.testelement.TestElement;

/**
 * 默认json断言
 *
 * @author xsx
 * @date 2022/8/29
 * @since 1.8
 * <p>
 * Copyright (c) 2022 xsx All Rights Reserved.
 * easy-jmeter is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * </p>
 */
@Data
@Accessors(chain = true)
public class JmeterDefaultJsonPathAssertion implements JmeterAssertion {

    /**
     * 测试类名称
     */
    public static final String TEST_CLASS_NAME = JSONPathAssertion.class.getName();
    /**
     * GUI类名称
     */
    public static final String GUI_CLASS_NAME = JSONPathAssertionGui.class.getName();

    /**
     * 断言名称
     */
    private String name;
    /**
     * 断言注释
     */
    private String comment;
    /**
     * json路径
     * <p>jsonPath语法：</p>
     * <p>$：表示根节点元素</p>
     * <p>@：表示当前所在位置节点元素</p>
     * <p>*：表示通配所有元素</p>
     * <p>.：表示匹配当前路径的下级节点</p>
     * <p>..：表示递归匹配当前路径的所有节点</p>
     * <p>[index]：表示匹配数组索引类型的节点</p>
     * <p>?(表达式)：对数据进行筛选</p>
     */
    private String jsonPath;
    /**
     * 期望值
     */
    private String expectedValue;
    /**
     * 是否期望值为空
     */
    private Boolean isExpectNull;
    /**
     * 是否添加期望值
     */
    private Boolean isAddExpectedValue;
    /**
     * 是否期望值为正则
     */
    private Boolean isRegexExpectedValue;
    /**
     * 是否断言反转（取反）
     */
    private Boolean isInvert;

    /**
     * 无参构造
     */
    private JmeterDefaultJsonPathAssertion() {
    }

    /**
     * 获取json断言实例
     *
     * @return 返回json断言实例
     */
    public static JmeterDefaultJsonPathAssertion getInstance() {
        return new JmeterDefaultJsonPathAssertion();
    }

    /**
     * 创建断言
     *
     * @return 返回断言
     */
    @Override
    public Assertion create() {
        // 创建json断言
        JSONPathAssertion jsonPathAssertion = new JSONPathAssertion();
        // 设置测试类名称
        jsonPathAssertion.setProperty(TestElement.TEST_CLASS, TEST_CLASS_NAME);
        // 设置GUI类名称
        jsonPathAssertion.setProperty(TestElement.GUI_CLASS, GUI_CLASS_NAME);
        // 设置启用
        jsonPathAssertion.setEnabled(true);
        // 设置断言名称
        jsonPathAssertion.setName(JmeterOptional.ofNullable(this.name).orElse("Json断言"));
        // 设置断言注释
        jsonPathAssertion.setComment(JmeterOptional.ofNullable(this.comment).orElse(""));
        // 设置json路径
        jsonPathAssertion.setJsonPath(JmeterOptional.ofNullable(this.jsonPath).orElse(""));
        // 设置期望值是否为空
        jsonPathAssertion.setExpectNull(JmeterOptional.ofNullable(this.isExpectNull).orElse(Boolean.FALSE));
        // 设置是否添加期望值
        jsonPathAssertion.setJsonValidationBool(JmeterOptional.ofNullable(this.isAddExpectedValue).orElse(Boolean.FALSE));
        // 设置是否期望值为正则
        jsonPathAssertion.setIsRegex(JmeterOptional.ofNullable(this.isRegexExpectedValue).orElse(Boolean.FALSE));
        // 设置期望值
        jsonPathAssertion.setExpectedValue(JmeterOptional.ofNullable(this.expectedValue).orElse(""));
        // 设置反转（取反）
        jsonPathAssertion.setInvert(JmeterOptional.ofNullable(this.isInvert).orElse(Boolean.FALSE));
        // 返回json断言
        return jsonPathAssertion;
    }
}

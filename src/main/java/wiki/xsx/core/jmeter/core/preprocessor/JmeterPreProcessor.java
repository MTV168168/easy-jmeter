package wiki.xsx.core.jmeter.core.preprocessor;

import org.apache.jmeter.processor.PreProcessor;

/**
 * 前置处理器
 *
 * @author xsx
 * @date 2022/9/2
 * @since 1.8
 * <p>
 * Copyright (c) 2022 xsx All Rights Reserved.
 * easy-jmeter is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * </p>
 */
public interface JmeterPreProcessor {

    /**
     * 创建前置处理器
     *
     * @return 返回前置处理器
     */
    PreProcessor create();
}

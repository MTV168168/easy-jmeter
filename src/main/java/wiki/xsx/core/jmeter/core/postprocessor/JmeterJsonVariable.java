package wiki.xsx.core.jmeter.core.postprocessor;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * json变量
 *
 * @author xsx
 * @date 2022/8/31
 * @since 1.8
 * <p>
 * Copyright (c) 2022 xsx All Rights Reserved.
 * easy-jmeter is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * </p>
 */
@Data
@Accessors(chain = true)
public class JmeterJsonVariable {

    /**
     * 变量名称
     */
    private String variableName;
    /**
     * json路径
     * <p>jsonPath语法：</p>
     * <p>$：表示根节点元素</p>
     * <p>@：表示当前所在位置节点元素</p>
     * <p>*：表示通配所有元素</p>
     * <p>.：表示匹配当前路径的下级节点</p>
     * <p>..：表示递归匹配当前路径的所有节点</p>
     * <p>[index]：表示匹配数组索引类型的节点</p>
     * <p>?(表达式)：对数据进行筛选</p>
     */
    private String jsonPath;
    /**
     * 匹配编号
     * <p>0表示随机，-1表示全部，1表示第一个，n表示第n个</p>
     */
    private Integer matchNo;
    /**
     * 默认值
     */
    private String defaultValue;

    /**
     * 无参构造
     */
    private JmeterJsonVariable() {
    }

    /**
     * 获取json变量实例
     *
     * @return 返回json变量实例
     */
    public static JmeterJsonVariable getInstance() {
        return new JmeterJsonVariable();
    }
}

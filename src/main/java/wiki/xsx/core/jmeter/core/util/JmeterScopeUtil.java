package wiki.xsx.core.jmeter.core.util;

import wiki.xsx.core.jmeter.core.enums.JmeterScope;
import org.apache.jmeter.testelement.AbstractScopedTestElement;

/**
 * 范围工具
 *
 * @author xsx
 * @date 2022/8/31
 * @since 1.8
 * <p>
 * Copyright (c) 2022 xsx All Rights Reserved.
 * easy-jmeter is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * </p>
 */
public class JmeterScopeUtil {

    /**
     * 初始化范围
     *
     * @param scope             范围
     * @param scopeVariableName 范围变量名称
     * @param testElement       测试元素
     */
    public static void initScope(JmeterScope scope, String scopeVariableName, AbstractScopedTestElement testElement) {
        // 判断断言范围
        switch (scope) {
            // 全部
            case ALL: {
                // 设置为全部
                testElement.setScopeAll();
                // 完成
                break;
            }
            // 仅主样本
            case ONLY_MAIN_SAMPLE: {
                // 设置为仅主样本
                testElement.setScopeParent();
                // 完成
                break;
            }
            // 仅子样本
            case ONLY_SUB_SAMPLE: {
                // 设置为子样本
                testElement.setScopeChildren();
                // 完成
                break;
            }
            // 变量
            case VARIABLE: {
                // 设置为变量
                JmeterOptional.ofNullable(scopeVariableName).ifPresent(testElement::setScopeVariable);
                // 完成
                break;
            }
            // 默认（仅主样本）
            default:
                // 设置为仅主样本
                testElement.setScopeParent();
        }
    }
}

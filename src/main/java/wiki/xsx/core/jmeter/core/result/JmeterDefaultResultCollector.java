package wiki.xsx.core.jmeter.core.result;

import wiki.xsx.core.jmeter.core.util.JmeterOptional;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.jmeter.visualizers.StatVisualizer;
import org.apache.jmeter.visualizers.SummaryReport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 默认结果收集器
 *
 * @author xsx
 * @date 2022/8/22
 * @since 1.8
 * <p>
 * Copyright (c) 2022 xsx All Rights Reserved.
 * easy-jmeter is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * </p>
 */
@Data
@Accessors(chain = true)
public class JmeterDefaultResultCollector implements JmeterResultCollector {

    /**
     * 保存目录
     */
    private String saveOutputDir;
    /**
     * 默认报告名称
     */
    private String defaultResultName;
    /**
     * 默认报告注释
     */
    private String defaultResultComment;

    /**
     * 结果收集器配置列表
     */
    private List<JmeterResultCollectorConfig> configs;

    /**
     * 无参构造
     */
    private JmeterDefaultResultCollector() {
    }

    /**
     * 获取结果收集器实例
     *
     * @return 返回结果收集器实例
     */
    public static JmeterDefaultResultCollector getInstance() {
        return new JmeterDefaultResultCollector();
    }

    /**
     * 初始化结果收集器配置容量
     *
     * @param initialCapacity 初始容量
     * @return 返回结果收集器
     */
    public JmeterDefaultResultCollector initialConfigCapacity(int initialCapacity) {
        this.configs = new ArrayList<>(initialCapacity);
        return this;
    }

    /**
     * 添加结果收集器配置
     *
     * @param  configs 结果收集器配置
     * @return 返回结果收集器
     */
    public JmeterDefaultResultCollector addResultCollector(JmeterResultCollectorConfig... configs) {
        // 初始化结果收集器配置列表
        this.initConfigs();
        // 添加结果收集器配置
        Collections.addAll(this.configs, configs);
        // 返回结果收集器
        return this;
    }

    /**
     * 开启聚合报告（仅支持导出时使用）
     *
     * @param name    报告名称
     * @param comment 报告注释
     * @return 返回结果收集器
     */
    public JmeterDefaultResultCollector enableAggregatedReport(String name, String comment) {
        // 初始化结果收集器配置列表
        this.initConfigs();
        // 添加到结果收集器配置
        this.configs.add(
                new JmeterDefaultResultCollectorConfig(
                        JmeterOptional.ofNullable(name).orElse("聚合报告"),
                        JmeterOptional.ofNullable(comment).orElse(""),
                        this.saveOutputDir,
                        STAT_VISUALIZER_GUI_CLASS_NAME,
                        StatVisualizer.class
                )
        );
        // 返回结果收集器
        return this;
    }

    /**
     * 开启汇总报告（仅支持导出时使用）
     *
     * @param name    报告名称
     * @param comment 报告注释
     * @return 返回结果收集器
     */
    public JmeterDefaultResultCollector enableSummaryReport(String name, String comment) {
        // 初始化结果收集器配置列表
        this.initConfigs();
        // 添加到结果收集器配置
        this.configs.add(
                new JmeterDefaultResultCollectorConfig(
                        JmeterOptional.ofNullable(name).orElse("汇总报告"),
                        JmeterOptional.ofNullable(comment).orElse(""),
                        this.saveOutputDir,
                        SUMMARY_REPORT_GUI_CLASS_NAME,
                        SummaryReport.class
                )
        );
        // 返回结果收集器
        return this;
    }

    /**
     * 获取结果收集器列表
     *
     * @return 返回结果收集器列表
     */
    @Override
    public List<JmeterResultCollectorConfig> getConfigs() {
        // 初始化结果收集器配置列表
        this.initConfigs();
        // 如果结果收集器配置列表为空，则添加默认收集器配置
        if (this.configs.isEmpty()) {
            // 添加默认收集器配置
            this.configs.add(
                    new JmeterDefaultResultCollectorConfig(
                            JmeterOptional.ofNullable(this.defaultResultName).orElse("测试报告"),
                            JmeterOptional.ofNullable(this.defaultResultComment).orElse(""),
                            this.saveOutputDir
                    )
            );
        }
        // 返回结果收集器配置列表
        return this.configs;
    }

    /**
     * 初始化结果收集器配置列表
     */
    private void initConfigs() {
        // 如果结果收集器配置列表未初始化，则初始化结果收集器配置列表
        if (this.configs == null) {
            // 初始化结果收集器配置列表
            this.initialConfigCapacity(10);
        }
    }
}

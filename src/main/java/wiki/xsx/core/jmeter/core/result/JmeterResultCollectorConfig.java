package wiki.xsx.core.jmeter.core.result;

import wiki.xsx.core.jmeter.core.overwrite.JmeterSummariser;
import org.apache.jmeter.reporters.ResultCollector;
import org.apache.jmeter.testelement.TestElement;

import java.util.Optional;

/**
 * 结果收集器配置
 *
 * @author xsx
 * @date 2022/8/24
 * @since 1.8
 * <p>
 * Copyright (c) 2022 xsx All Rights Reserved.
 * easy-jmeter is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * </p>
 */
@FunctionalInterface
public interface JmeterResultCollectorConfig {

    /**
     * 测试类名称
     */
    String TEST_CLASS_NAME = ResultCollector.class.getName();

    /**
     * 创建收集器
     *
     * @return 返回结果收集器
     */
    ResultCollector create();

    /**
     * 创建结果收集器
     *
     * @param guiClassName GUI类名称
     * @param name         收集器名称
     * @param comment      收集器注释
     * @return 返回结果收集器
     */
    default ResultCollector create(String guiClassName, String name, String comment) {
        // 创建结果收集器
        ResultCollector resultCollector = new ResultCollector(new JmeterSummariser(name));
        // 设置测试类名称
        resultCollector.setProperty(TestElement.TEST_CLASS, TEST_CLASS_NAME);
        // 如果GUI类名称不为空，则设置GUI类名称
        Optional.ofNullable(guiClassName).ifPresent(gui -> resultCollector.setProperty(TestElement.GUI_CLASS, gui));
        // 设置启用
        resultCollector.setEnabled(true);
        // 设置收集器名称
        Optional.ofNullable(name).ifPresent(resultCollector::setName);
        // 设置收集器注释
        Optional.ofNullable(comment).ifPresent(resultCollector::setComment);
        // 返回结果收集器
        return resultCollector;
    }
}

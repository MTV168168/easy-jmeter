package wiki.xsx.core.jmeter.core.sampler;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.http.control.HeaderManager;
import org.apache.jmeter.protocol.http.control.gui.HttpTestSampleGui;
import org.apache.jmeter.protocol.http.gui.HTTPArgumentsPanel;
import org.apache.jmeter.protocol.http.gui.HeaderPanel;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerProxy;
import org.apache.jorphan.collections.HashTree;

/**
 * http采样器配置
 *
 * @author xsx
 * @date 2022/8/24
 * @since 1.8
 * <p>
 * Copyright (c) 2022 xsx All Rights Reserved.
 * easy-jmeter is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * </p>
 */
@FunctionalInterface
public interface JmeterHttpSamplerConfig {

    /**
     * 请求头测试类名称
     */
    String HEADER_TEST_CLASS_NAME = HeaderManager.class.getName();
    /**
     * 请求头GUI类名称
     */
    String HEADER_GUI_CLASS_NAME = HeaderPanel.class.getName();
    /**
     * 请求代理测试类名称
     */
    String PROXY_TEST_CLASS_NAME = HTTPSamplerProxy.class.getName();
    /**
     * 请求代理GUI类名称
     */
    String PROXY_GUI_CLASS_NAME = HttpTestSampleGui.class.getName();
    /**
     * 请求参数测试类名称
     */
    String ARGUMENTS_TEST_CLASS_NAME = Arguments.class.getName();
    /**
     * 请求参数GUI类名称
     */
    String ARGUMENTS_GUI_CLASS_NAME = HTTPArgumentsPanel.class.getName();

    /**
     * 创建http采样器
     *
     * @return 返回http采样器配置树
     */
    HashTree create();
}

package wiki.xsx.core.jmeter.core.result;

import wiki.xsx.core.jmeter.core.util.JmeterOptional;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import org.apache.jmeter.reporters.ResultCollector;
import org.apache.jmeter.visualizers.Visualizer;

import java.io.File;

/**
 * 默认结果收集器配置
 *
 * @author xsx
 * @date 2022/8/24
 * @since 1.8
 * <p>
 * Copyright (c) 2022 xsx All Rights Reserved.
 * easy-jmeter is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * </p>
 */
@Data
@Accessors(chain = true)
public class JmeterDefaultResultCollectorConfig implements JmeterResultCollectorConfig {

    /**
     * 报告名称
     */
    private String name;
    /**
     * 报告注释
     */
    private String comment;
    /**
     * 报告保存目录
     */
    private String saveOutputDir;
    /**
     * GUI类名称
     */
    private String guiClassName;
    /**
     * 监听器类型
     */
    private Class<? extends Visualizer> classType;

    /**
     * 有参构造
     *
     * @param name          报告名称
     * @param comment       报告注释
     * @param saveOutputDir 报告保存目录
     */
    public JmeterDefaultResultCollectorConfig(String name, String comment, String saveOutputDir) {
        this.name = name;
        this.comment = comment;
        this.saveOutputDir = saveOutputDir;
    }

    /**
     * 有参构造
     *
     * @param name          报告名称
     * @param comment       报告注释
     * @param saveOutputDir 报告保存目录
     * @param guiClassName  GUI类名称
     * @param classType     监听器类型
     */
    JmeterDefaultResultCollectorConfig(String name, String comment, String saveOutputDir, String guiClassName, Class<? extends Visualizer> classType) {
        this.name = name;
        this.comment = comment;
        this.saveOutputDir = saveOutputDir;
        this.guiClassName = guiClassName;
        this.classType = classType;
    }

    /**
     * 创建收集器
     *
     * @return 返回结果收集器
     */
    @SneakyThrows
    @Override
    public ResultCollector create() {
        // 创建结果收集器
        ResultCollector resultCollector = this.create(this.guiClassName, this.name, this.comment);
        // 如果监听器类型不为空，则设置监听器
        JmeterOptional.ofNullable(this.classType).ifPresent(type -> this.setListener(type, resultCollector));
        // 如果样本保存路径不为空，则添加样本保存路径
        JmeterOptional.ofNullable(this.saveOutputDir).ifPresent(dir -> resultCollector.setFilename(new File(dir, this.name + ".xml").getAbsolutePath()));
        // 返回结果收集器
        return resultCollector;
    }

    /**
     * 设置监听器
     *
     * @param classType       监听器类型
     * @param resultCollector 结果收集器
     */
    @SneakyThrows
    private void setListener(Class<? extends Visualizer> classType, ResultCollector resultCollector) {
        resultCollector.setListener(classType.newInstance());
    }
}

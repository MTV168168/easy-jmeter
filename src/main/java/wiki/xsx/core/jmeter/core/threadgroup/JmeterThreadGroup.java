package wiki.xsx.core.jmeter.core.threadgroup;

import org.apache.jmeter.threads.ThreadGroup;
import org.apache.jmeter.threads.gui.ThreadGroupGui;
import org.apache.jorphan.collections.HashTree;

/**
 * 线程组
 *
 * @author xsx
 * @date 2022/8/24
 * @since 1.8
 * <p>
 * Copyright (c) 2022 xsx All Rights Reserved.
 * easy-jmeter is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * </p>
 */
@FunctionalInterface
public interface JmeterThreadGroup {

    /**
     * 测试类名称
     */
    String TEST_CLASS_NAME = ThreadGroup.class.getName();
    /**
     * GUI类名称
     */
    String GUI_CLASS_NAME = ThreadGroupGui.class.getName();

    /**
     * 创建线程组
     *
     * @return 返回线程组配置树
     */
    HashTree create();
}

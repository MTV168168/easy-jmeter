package wiki.xsx.core.jmeter.core.assertion;

import wiki.xsx.core.jmeter.core.enums.JmeterAssertionMatchMode;
import wiki.xsx.core.jmeter.core.enums.JmeterAssertionTestField;
import org.apache.jmeter.assertions.Assertion;
import org.apache.jmeter.assertions.ResponseAssertion;

/**
 * 断言
 *
 * @author xsx
 * @date 2022/8/29
 * @since 1.8
 * <p>
 * Copyright (c) 2022 xsx All Rights Reserved.
 * easy-jmeter is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * </p>
 */
public interface JmeterAssertion {

    /**
     * 创建断言
     *
     * @return 返回断言
     */
    Assertion create();

    /**
     * 初始化测试字段
     *
     * @param testField         测试字段
     * @param responseAssertion 响应断言
     */
    default void initTestField(JmeterAssertionTestField testField, ResponseAssertion responseAssertion) {
        // 判断测试字段
        switch (testField) {
            // 响应数据
            case RESPONSE_DATA: {
                // 设置测试字段为响应数据
                responseAssertion.setTestFieldResponseData();
                // 完成
                break;
            }
            // 响应文档
            case RESPONSE_DATA_AS_DOCUMENT: {
                // 设置测试字段为响应文档
                responseAssertion.setTestFieldResponseDataAsDocument();
                // 完成
                break;
            }
            // 响应码
            case RESPONSE_CODE: {
                // 设置测试字段为响应码
                responseAssertion.setTestFieldResponseCode();
                // 完成
                break;
            }
            // 响应信息
            case RESPONSE_MESSAGE: {
                // 设置测试字段为响应信息
                responseAssertion.setTestFieldResponseMessage();
                // 完成
                break;
            }
            // 响应头
            case RESPONSE_HEADERS: {
                // 设置测试字段为响应头
                responseAssertion.setTestFieldResponseHeaders();
                // 完成
                break;
            }
            // 请求数据
            case REQUEST_DATA: {
                // 设置测试字段为请求数据
                responseAssertion.setTestFieldRequestData();
                // 完成
                break;
            }
            // 请求路径
            case REQUEST_URL: {
                // 设置测试字段为请求路径
                responseAssertion.setTestFieldURL();
                // 完成
                break;
            }
            // 请求头
            case REQUEST_HEADERS: {
                // 设置测试字段为请求头
                responseAssertion.setTestFieldRequestHeaders();
                // 完成
                break;
            }
            // 默认（响应数据）
            default:
                // 设置测试字段为响应数据
                responseAssertion.setTestFieldResponseData();
        }
    }

    /**
     * 初始化匹配模式
     *
     * @param matchMode         匹配模式
     * @param responseAssertion 响应断言
     */
    default void initMatchMode(JmeterAssertionMatchMode matchMode, ResponseAssertion responseAssertion) {
        // 判断匹配模式
        switch (matchMode) {
            // 字符串
            case STRING: {
                // 设置匹配模式为字符串
                responseAssertion.setToSubstringType();
                // 完成
                break;
            }
            // 包含
            case CONTAINS: {
                // 设置匹配模式为包含
                responseAssertion.setToContainsType();
                // 完成
                break;
            }
            // 匹配
            case MATCH: {
                // 设置匹配模式为匹配
                responseAssertion.setToMatchType();
                // 完成
                break;
            }
            // 等于
            case EQUALS: {
                // 设置匹配模式为等于
                responseAssertion.setToEqualsType();
                // 完成
                break;
            }
            // 默认（字符串）
            default:
                // 设置匹配模式为字符串
                responseAssertion.setToSubstringType();
        }
    }
}
